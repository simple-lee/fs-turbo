alter table fs_car change file_type sences varchar(20) not null;
drop index ik_fs_car_bus_id on fs_car;
drop index ik_fsc_crc on fs_car;
rename table fs_car to fs_turbo;
create index ik_fs_turbo_bus_id
	on fs_turbo (bus_id);
create index ik_fs_turbo_crc
	on fs_turbo (crc);