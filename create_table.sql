-- 暂时就适配mysql
create table fs_turbo
(
    id             char(24)     not null
        primary key,
    inner_type     int          not null comment '内部类型$目前支持0-普通附件,1-图片',
    scenes      varchar(20)  not null,
    bus_id         varchar(32)  null,
    source         bit          not null,
    crc            bigint       not null,
    md5            char(32)     not null,
    create_time    datetime     not null,
    save_file_name varchar(255) not null,
    file_name      varchar(255) not null,
    path           varchar(255) not null
);

create index ik_fs_turbo_bus_id
    on fs_turbo (bus_id);

create index ik_fsc_crc
    on fs_turbo (crc);



