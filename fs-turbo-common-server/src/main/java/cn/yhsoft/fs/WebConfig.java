package cn.yhsoft.fs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author LiYong
 */
@Configuration
public class WebConfig {
    @Bean
    public FsStorageAdapter fsStorageAdapter() {
        return new GoFastDFSStorageAdapter("http://127.0.0.1:8080/");
//        return new FsFileSystemStorageAdapter("/data");
    }
}
