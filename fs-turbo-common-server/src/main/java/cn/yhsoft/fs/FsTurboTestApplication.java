package cn.yhsoft.fs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author LiYong
 */
@SpringBootApplication(scanBasePackages = {
        "cn.yhsoft.fs"
})
public class FsTurboTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(FsTurboTestApplication.class, args);
    }
}
