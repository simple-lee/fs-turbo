# FS-Turbo

#### 介绍
FS-Turbo, Java web简单、快速、高性能附件解决方案 
FS-Turbo, Simple, fast and high-performance attachment solution for Java web

#### 功能
- 提供文件上传下载，图片上传、缩略图生成、下载、预览接口
    - 提供基于Restful WEB API的
    - 提供基于Spring Component的Service接口
- 不仅仅是Java语言，也可以作为通用的文件服务端使用

#### API清单
1. 上传 POST /upload
- 参数 form-data: ```sences``` ```busId``` ```file``` ```multi```
    - ```sences```: 场景，可不传入，不传入则默认为default。用户自行进行定义,一般使用所属业务简称
    - ```busId```: 业务id，可不传入
    - ```file```: 文件，必须传入
    - ```multi```: 是否多文件，默认为false，可不传入，必须配套busId使用，为true表示对应的busId可以上传多个文件,为false则进行覆盖上传
- 返回 json: ```code``` ```id``` ```message```
    - ```code```: 200表示成功
    - ```id```: 文件服务生成唯一id，objectId类型，26位
    - ```message```: 如果失败则返回错误消息
2. 删除 POST /del
- 参数 json payload: id
    - ```id```: 文件id
- 返回 json: ```code``` ```id``` ```message```
    - ```code```: 200表示成功
    - ```id```: null
    - ```message```: 如果失败则返回错误消息
3. 下载 GET /download/{id}
- 参数 path param: ```id```
    - ```id```: 文件id
4. 预览 GET /preview/{id}
- 参数 path param: ```id```
    - ```id```: 文件id
5. 获取业务对应图片列表 /list/{sences}/{bus-id}
- 参数 path param: ```sences``` ```bus-id```
    - ```sences```: 场景
    - ```bus-id```: 业务id
- 返回 ```array<string>```，如存在则返回文件id数组，如不存在则返回空数组```[]```
6. 图片上传 POST /image/upload
参数及返回同 /upload 接口
7. 图片预览 GET /image/preview/{id}
- 参数 path param: ```id```
    - ```id```: 图片id
8. 缩略图预览 GET /image/thumb/{id}
- 参数 path param: ```id```
    - ```id```: 图片id
9. 根据业务预览图片 GET /image/preview/bus/{sences}/{bus-id}
- 参数 path param: ```sences``` ```bus-id```
    - ```sences```: 场景
    - ```bus-id```: 业务id
#### 应用场景
1. 一个业务id只有一个附件/图片
- 可以采用两种设计模式：业务侵入性、业务无侵入性(建议)
    - 侵入性模式：在业务记录上增加一个字段用于存储附件id
    - 无侵入性模式：使用场景(sences)+业务id(bus-id)方式进行文件上传
    但无侵入性模式存在缺陷：必须先存在业务id，对于新增场景有一定局限性
    **对于新增场景**，业务侧不能设计为数据库自动生成id，而是点击新增后，新增界面从后台获取一个id作为业务id使用。
- 业务数据删除，以下两种模式根据设计需求进行选择
    - 可以调用 /del 接口同步删除附件，也可以同步在业务侧集成fs-turbo，使用service进行事务性删除
    - 可以不删除附件，降低代码量和异常概率，但存在文件冗余
2. 一个业务id有多个附件
- 可以采用两种设计模式：业务侵入性、业务无侵入性(建议)
    - 侵入性模式：在业务记录上增加一个长字段用于存储多个附件id并使用分隔符如","隔开，或者增加明细关联表
    - 非侵入性模式：使用场景(sences)+业务id(bus-id)+多文件模式(multi)方式进行文件上传，新增场景同单附件。
        - 多图片预览：调用```/list/{sences}/{bus-id}```接口，返回id-list，通过```/image/thumb/{id}```或```/image/preview/{id}```进行列表
#### 部署/使用模式
##### 嵌入式使用
```xml
<dependency>
    <groupId>cn.yhsoft</groupId>
    <artifactId>fs-turbo</artifactId>
    <version>${version}</version>
</dependency>
```
##### 通用服务器(推荐)
###### 准备工作
1. git clone项目到本地
2. 创建mysql数据库并通过```create_table.sql```创建所需数据库表
3. 调整common-server配置文件适配器、数据库配置、WebContext信息
- WebConfig.java
选择使用本地适配器或其他文件适配器，详见示例代码
- application.properties
```
# 以下列举重要配置参数，其他参数详见文件注释
spring.datasource.url=jdbc:xxx
spring.datasource.username=root
spring.datasource.password=
#访问controller是的前缀,如配置为file则上传文件的地址为 /file/upload
fs-turbo.context=file
```
4. build项目 ```mvn clean install```
5. 启动common-server ```java -jar fs-turbo-common-server-x.x.x.jar```
###### 使用
> 使用API详见[#### API清单]
> 以下仅说明基数使用模式，不包含集中鉴权
1. 开发模式，配合vite、webpack等，在项目中配置反向代理，如下示例
```
proxy: {
    '/api/file': {
      target: 'http://127.0.0.1:8088',
      changeOrigin: true,
      rewrite: (path) => path.replace(/^\/api\/file/, '/file/')
    },
    '/api': {
      target: config.VITE_SERVICE_IP,
      changeOrigin: true,
      rewrite: (path) => path.replace(/^\/api/, '')
    }
}
```
2. 部署模式，配合nginx使用，如下示例
```
location /api/file/ {
    proxy_pass http://127.0.0.1:8088/file/;
    proxy_http_version     1.1;
    proxy_set_header Upgrade                    $http_upgrade;
    proxy_set_header Host                       $host:$server_port;
    proxy_set_header X-Real-IP                  $remote_addr;
    proxy_set_header X-Forwarded-For    $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto  $scheme;
    proxy_set_header X-Forwarded-Host   $host;
    proxy_set_header X-Forwarded-Port   $server_port;
}
```

##### 高性能部署模式
TODO
#### 存储适配器
1. 本地文件（默认）```FsFileSystemStorageAdapter```
2. go-fastdfs```GoFastDFSStorageAdapter```