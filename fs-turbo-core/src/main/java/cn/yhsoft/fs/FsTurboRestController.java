package cn.yhsoft.fs;

import java.lang.annotation.*;

/**
 * @author LiYong
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE, ElementType.METHOD})
@Documented
public @interface FsTurboRestController {
}
