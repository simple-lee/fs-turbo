package cn.yhsoft.fs;

import java.io.InputStream;

/**
 * @author LiYong
 * 文件存储适配器接口，建议支持本地文件系统、hdfs、fastdfs、对象存储等
 */
public interface FsStorageAdapter {
    /**
     * 保存文件
     *
     * @param path            需要保存文件的路径
     * @param saveFileName    存储的文件名
     * @param fileInputStream 文件流
     */
    String save(String path, String saveFileName, InputStream fileInputStream);

    /**
     * 根据path路径获取到源文件文件流
     * @param path 存储的文件路劲（含文件名）
     * @return
     */
    InputStream getInputStream(String path);
}
