package cn.yhsoft.fs.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author LiYong
 */
@Component
@ConfigurationProperties(prefix = "fs-turbo")
@Data
public class FsTurboProperties {
    /**
     * file trans controller path prefix
     */
    String context = "file";
    /**
     * auto generate image thumb size
     */
    Integer imageThumbSize=64;
    /**
     * max image file size,default 1M
     */
    Integer imageSizeMax=1048576;
    /**
     * max file size,default 1M
     */
    Integer fileSizeMax=1048576;
    /**
     * allowed image file extension
     */
    String[] allowImageFileExtension=new String[]{"jpg", "jpeg", "png", "bmp"};
}
