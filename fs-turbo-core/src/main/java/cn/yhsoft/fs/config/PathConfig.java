package cn.yhsoft.fs.config;

import cn.yhsoft.fs.FsTurboRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author LiYong
 */
@Configuration
public class PathConfig implements WebMvcConfigurer {
    @Autowired
    private FsTurboProperties fsTurboProperties;
    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.addPathPrefix(fsTurboProperties.context, c -> c.isAnnotationPresent(FsTurboRestController.class));
    }
}
