package cn.yhsoft.fs.gofastdfs;

import lombok.Data;

import java.io.Serializable;

/*
{
  "data": null,
  "message": "http: no such file",
  "status": "fail"
}

{
  "domain": "http://127.0.0.1:8080",
  "md5": "84e41970dbe22663e97b0a79a02f0d00",
  "mtime": 1647588255,
  "path": "/group1/post/2022/03/18/623433a088f605130dda7135.png",
  "retcode": 0,
  "retmsg": "",
  "scene": "default",
  "scenes": "default",
  "size": 2078,
  "src": "/group1/post/2022/03/18/623433a088f605130dda7135.png",
  "url": "http://127.0.0.1:8080/group1/post/2022/03/18/623433a088f605130dda7135.png?name=623433a088f605130dda7135.png&download=1"
}
 */

/**
 * GoFastDFS 文件上传返回结构json对应字段
 * <remark>当前适配版本v1.4.3</remark>
 * @author LiYong
 */
@Data
public class GoFastDFSResult implements Serializable {
    private Integer retcode;
    /**
     * 失败可能有这个字段
     */
    private String status;
    /**
     * 失败可能有这个字段
     */
    private String message;
    private String retmsg;
    private String scenes;
    private String path;

    public static String STATUS_FAIL="fail";
}