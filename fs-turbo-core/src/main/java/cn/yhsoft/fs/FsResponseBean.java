package cn.yhsoft.fs;

import lombok.Data;

/**
 * @author LiYong
 */
@Data
public class FsResponseBean {
    /**
     * 反馈代码，暂仅支持200-成功 500-服务器内部错误
     * 当反馈500错误的时候可以通过message获取错误消息
     */
    private int code;
    /**
     * 上传之后的文件id
     */
    private String id;
    private String message;
}
