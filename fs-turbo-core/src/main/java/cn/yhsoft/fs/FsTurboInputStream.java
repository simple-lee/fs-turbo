package cn.yhsoft.fs;

import lombok.Builder;
import lombok.Data;

import java.io.InputStream;

@Data
@Builder
public class FsTurboInputStream {
    private InputStream stream;
    private FsTurboDto dto;
}
