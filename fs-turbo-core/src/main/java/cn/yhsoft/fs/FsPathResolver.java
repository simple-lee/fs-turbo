package cn.yhsoft.fs;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author LiYong
 * 文件存储路径处理器
 * 默认情况下为 {type}/{yyyy}/{mm}/{dd}
 */
@Component
public class FsPathResolver {
    public String getSavePath(String type, Date createTime) {
        return type + "/" + DateUtil.format(createTime, "yyyy/MM/dd");
    }

    public String getThumbSavePath(String type, Date createTime) {
        return type + "/thumbs/" + DateUtil.format(createTime, "yyyy/MM/dd");
    }

    public String getThumbSavePath(String path) {
        return StrUtil.builder().append(path, 0, path.indexOf("/")).append("/thumbs").append(path.substring(path.indexOf("/"))).toString();
    }
}
