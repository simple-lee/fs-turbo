package cn.yhsoft.fs;

/**
 * @author LiYong
 */
public class FsInnerTypes {
    /**
     * 默认
     */
    public final static int DEFAULT = 0;
    /**
     * 图片
     */
    public final static int IMAGE = 1;
}
