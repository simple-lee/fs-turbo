package cn.yhsoft.fs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author LiYong
 * 存储到数据库持久化的dto
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FsTurboDto implements Serializable {
    private String id;
    /**
     * 场景，必须传入
     */
    private String scenes = "default";
    private Integer innerType;
    private String busId;
    private boolean source;
    private String md5;
    private Timestamp createTime;
    /**
     * 存储的文件名
     */
    private String saveFileName;
    /**
     * 原始文件名
     */
    private String fileName;
    /**
     * 文件存储的全路劲
     */
    private String path;
}
